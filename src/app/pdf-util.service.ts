import { Injectable } from '@angular/core';
import { Paths } from '../Models/paths';

//Pdf make import:
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

enum sudokuType {
  SINGLE = "SINGLE",
  DOUBLE = "DOUBLE",
  PYRAMID = "PYRAMID",
  SAMURAI = "SAMURAI"
}

@Injectable({
  providedIn: 'root'
})

export class PdfUtilService {

  public pdfObj = null;
  public pdfObjSolutions = null;
  public finalCustomNumbers = [];

    //Image Paths:
    public imagePaths:any = new Paths();
    public topBorder:any;
    public bottomBorder:any;

  constructor() { }
  
  downloadPdf(canvas, canvasSolution, title, numberOfSudoku, sudokuType){
    this.topBorder = this.imagePaths.topBorder1;
    this.bottomBorder = this.imagePaths.bottomBorder;
    this.finalizeSudokuDocument(canvas, 0, numberOfSudoku, sudokuType);
    this.finalizeSudokuDocument(canvasSolution, 1, numberOfSudoku, sudokuType);
    this.pdfObj.download('Sudoku No '+ numberOfSudoku);
    this.pdfObjSolutions.download('Sudoku Solution '+ numberOfSudoku);
    this.finalCustomNumbers = [];
  }


  finalizeSudokuDocument(canvas, num, numberOfSudoku, sudokuShape){
    let customDivider:string = '\n\n';
    let titleHeaderPadding:string = '15%';
    let puzzleWord = this.imagePaths.puzzleWord2;
    this.convertNumToImages(numberOfSudoku);
    console.log('number of sudoku ' , numberOfSudoku);
    let headerImgWidth:number = 110;
    let headerImgHeight:number = 50;
    let headerString:string = '30%';
    let numberString:string = '3%';
    let numberHeight:number = 50;
    let numberWidth:number = 40;
    let separatorString:string = '4%';
    let canvasWidth:number = 500;
    let canvasHeight:number = 480;
    let canvasMarginLeft:number = 0;
    let bottomBorderTop:number = 20;
    let bottomBorderBottom:number = 20;
    console.log('sudoku type: ', sudokuShape);
    console.log(sudokuShape);
    switch(sudokuShape){
      case sudokuType.SINGLE:{
        console.log('SINGLE SUDOKU');
        canvasHeight = 540;
        canvasWidth = 580;
        canvasMarginLeft = -40;
        break;
      }
      case sudokuType.DOUBLE:{
        console.log('DOUBLE SUDOKU');
        canvasHeight = 590;
        canvasWidth = 640;
        canvasMarginLeft = -60;
        bottomBorderTop = 5;
        bottomBorderBottom = 5;
        break;
      }
      case sudokuType.PYRAMID: {
        console.log('PYRAMID SUDOKU');
        canvasHeight = 570;
        canvasWidth = 590;
        canvasMarginLeft = -38;
        bottomBorderTop = 5;
        bottomBorderBottom = 5;
      //  customDivider = '\n\n\n\n';

        break;
      }
      case sudokuType.SAMURAI: {
        console.log('SAMURAI SUDOKU');
        canvasHeight = 570;
        canvasWidth = 610;
        canvasMarginLeft = -48;
        bottomBorderTop = 5;
        bottomBorderBottom = 5;
        customDivider = '\n\n\n\n';
        break;
      }
      default: {
        console.log('whaaat');
      }
    }

    var docDefinition = {
      content: [
        //Top Border:
          {
          style: 'section',
           table: {
            widths: ['9%', '80%',  '*'],
            body:[
              [
                {
                   text:''
                },
                {
                    image: this.topBorder,
                    margin: [2, 20, 0, 20],
                    alignment: 'center'
                },
                {
                  text: ''
                },
              ]
            ]
          },
          layout: 'noBorders'
        },
         // Main Header
         {
          style: 'section',
           table: {
            widths: ['*', headerString, numberString, numberString, numberString, numberString,  '*'],
            body:[
              [
                {
                   text:''
                },
                {
                  image: puzzleWord,
                  width: headerImgWidth,
                  height: headerImgHeight,
                },

                {
                  image: this.finalCustomNumbers[0],
                  width: numberWidth,
                  height: numberHeight
                },
                {
                  image: this.finalCustomNumbers[1],
                  width: numberWidth,
                  height: numberHeight
                },
                {
                  image: this.finalCustomNumbers[2],
                  width: numberWidth,
                  height: numberHeight
                },
                {
                  image: this.finalCustomNumbers[3],
                  width: numberWidth,
                  height: numberHeight
                },
                {
                  text: ''
                },
              ]
            ]
          },
          layout: 'noBorders'
        },
        //Divider
        {
          text: customDivider
        },
        //Findaword Canvas
         {
          style: 'section',
           table: {
            widths: ['0.0000001%', '*', '*'],
            body:[
              [
                {
                    text: ''
                },
                {
                  image: canvas, 
                  width: canvasWidth,
                  height: canvasHeight,
                  margin: [canvasMarginLeft, -70, 0, 0]
                },
                {
                   text: ''
                },
              ]
            ]
          },
          layout: 'noBorders'
        },
    // {
    //     text: ''
    // },

    {
      style: 'section',
       table: {
        widths: ['9.3%', '80%',  '*'],
        body:[
          [
            {
               text:''
            },
            {
                image: this.bottomBorder,
                margin: [2, bottomBorderTop, 0, bottomBorderBottom],
                alignment: 'center'
            },
            {
              text: ''
            },
          ]
        ]
      },
      layout: 'noBorders'
    },
    
      
      ],
      //==============================================
      
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          background: 'red',
        },
        date: {
          fontSize: 15,
          bold: false,
          color: 'white',
          alignment: 'center',
        },
        section: {
          // fillColor: '#2BA8F1'
        },
        subheader: {
          fontSize: 14,
          bold:true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }
    }
    if(num == 0)
      this.pdfObj = pdfMake.createPdf(docDefinition);
    if(num == 1)
      this.pdfObjSolutions = pdfMake.createPdf(docDefinition);
  }


  convertNumToImages(num){
    let content:string = num.toString();
    console.log(content + ' is the content');
    console.log(content.length + ' is the length of content')
    switch(content.length)
    {
      case 1: {
        this.finalCustomNumbers.push(this.numberSwitch(content));
        this.finalCustomNumbers.push(this.imagePaths.empty);
        this.finalCustomNumbers.push(this.imagePaths.empty);
        this.finalCustomNumbers.push(this.imagePaths.empty);
        break;
      }
      case 2: {
        this.finalCustomNumbers.push(this.numberSwitch(content[0]));
        this.finalCustomNumbers.push(this.numberSwitch(content[1]));
        this.finalCustomNumbers.push(this.imagePaths.empty);
        this.finalCustomNumbers.push(this.imagePaths.empty);
        break;
      }
      case 3: {
        this.finalCustomNumbers.push(this.numberSwitch(content[0]));
        this.finalCustomNumbers.push(this.numberSwitch(content[1]));
        this.finalCustomNumbers.push(this.numberSwitch(content[2]));
        this.finalCustomNumbers.push(this.imagePaths.empty);
        break;
      }
      case 4: {
        this.finalCustomNumbers.push(this.numberSwitch(content[0]));
        this.finalCustomNumbers.push(this.numberSwitch(content[1]));
        this.finalCustomNumbers.push(this.numberSwitch(content[2]));
        this.finalCustomNumbers.push(this.numberSwitch(content[3]));
        break;
      }
      default: {
        console.log('you fucked upppppp');
        break;
      }
    }
  }
  numberSwitch(content){
    let num:number = parseInt(content);
    switch(num){
      case 0:{
        console.log('numberswitch0');
        return this.imagePaths.numberZero;
      }
      case 1:{
        console.log('numberswitch1');
        return this.imagePaths.numberOne;
      }
      case 2:{
        console.log('numberswitch2');
        return this.imagePaths.numberTwo;
      }
      case 3: {
        console.log('numberswitch3');
        return this.imagePaths.numberThree;
      }
      case 4: {
        console.log('numberswitch4');
        return this.imagePaths.numberFour;
      }
      case 5: {
        console.log('numberswitch5');
        return this.imagePaths.numberFive;
      }
      case 6: {
        return this.imagePaths.numberSix;
      }
      case 7: {
        return this.imagePaths.numberSeven;
      }
      case 8: {
        return this.imagePaths.numberEight;
      }
      case 9: {
        return this.imagePaths.numberNine;
      }
      default: {
        console.log('you fucked up my friend');
        break;
      }
    }
  }
}

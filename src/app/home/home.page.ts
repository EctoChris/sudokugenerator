import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Sudokunumber } from '../../Models/sudokunumber';
import  { LetterUtilService } from '../letter-util.service';
import { Sudoku } from '../../Models/sudoku';
import { PdfUtilService } from '../pdf-util.service';

import asyncLoop from 'node-async-loop';

//Pdf make import:
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { SWITCH_INJECTOR_FACTORY__POST_R3__ } from '@angular/core/src/di/injector';
import { hasLifecycleHook } from '@angular/compiler/src/lifecycle_reflector';
import { format } from 'url';
// import { create } from 'domain';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

enum startingGrid {
  TOPRIGHT = "TOPRIGHT",
  BOTTOMRIGHT = "BOTTOMRIGHT",
  BOTTOMLEFT = "BOTTOMLEFT",
  TOPLEFT = "TOPLEFT"
}

enum sudokuType {
  SINGLE = "SINGLE",
  DOUBLE = "DOUBLE",
  PYRAMID = "PYRAMID",
  SAMURAI = "SAMURAI"
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  @ViewChild('canvas') canvasEl : ElementRef;
  @ViewChild('canvasSolution') canvasSolutionEl : ElementRef;

  //UI Data Gathering:
  private canvas: any;
  private canvasSolution: any;
  private outputCanvas: any;
  private outputSolutionCanvas: any;
  private ctx : any;
  private ctx2 : any;
  private sudokuShapeSelected:number;
  public something:sudokuType = sudokuType.SINGLE;
  public sudokusToDownload:number = 1;

  //Grid:
  public rows =    [0,1,2,3,4,5,6,7,8];
  public columns = [0,1,2,3,4,5,6,7,8];
  public sudokuHeight:number = 1000;
  public sudokuWidth:number = 1000;
  public grid:any = [];
  public tempGrid:any = [];
  public solutionsGrid:any = [];
  public startingGrid:any = null;
  public solutionsCount:number = 0;
  public lastCoordRemoved = [];
  public lastNumRemoved:number = 0;
  public singleSudoku:Sudoku;
  public secondSudoku:Sudoku;
  public thirdSudoku:Sudoku;
  public fourthSudoku:Sudoku;
  public fifthSudoku:Sudoku;
  public singleSudokuChosen:boolean = true;
  public doubleSudokuChosen:boolean = false;
  public pyramidSudokuChosen:boolean = false;
  public samuraiSudokuChosen:boolean = false;

  //PDF:
  pdfObj = null;
  pdfObjSolutions = null;

  constructor(private renderer:Renderer, private LU: LetterUtilService,  private PDF: PdfUtilService) {

  }
  
  ngOnInit(){
    this.singleSudoku   = new Sudoku(this.LU, startingGrid.TOPLEFT, true, null, 0);
    this.canvas	        = this.canvasEl.nativeElement;
    this.canvasSolution = this.canvasSolutionEl.nativeElement;
    this.canvas.width   = this.canvasSolution.width = this.sudokuWidth + (0.1 * this.sudokuWidth);
    this.canvas.height  = this.canvasSolution.height = this.sudokuHeight;

    this.setupCanvas();
    // this.setupGrid();
    this.generateSudoku();
  }

  downloadSudoku(){
      let obj:any = [];
      for(let i:number = 1; i <= this.sudokusToDownload; i++)
      {
        obj.push(i);
      }
      // asyncLoop([0,1,2], async (i, next) =>
      asyncLoop(obj, async (i, next) =>
      {
        await this.delay(500);
        this.generateSudoku();
        this.outputCanvas = this.canvas.toDataURL();
        this.outputSolutionCanvas = this.canvasSolution.toDataURL();
        this.PDF.downloadPdf(this.outputCanvas, this.outputSolutionCanvas, 'Sudoku', i, this.something);  
        this.test();
        next();
      }, function ()
      {
          console.log('Finished!');
      });  
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
  test(){
    console.log('test works');
  }

  generateSudoku(){
    this.clearSudoku();

    switch(this.something){
      case sudokuType.SINGLE: {
        this.singleSudoku.setupGrid();
        this.singleSudoku.generateSudoku();
        break;
      } 
      case sudokuType.DOUBLE: {
        this.singleSudoku.setupGrid();
        this.singleSudoku.generateSudoku();
        let secondStartingGrid:startingGrid = null;
        let startingSquareArray:any = null;
        let rand:number = Math.floor(Math.random() * 2);
        if(rand == 0){
          secondStartingGrid = startingGrid.TOPLEFT;
          startingSquareArray = this.singleSudoku.retrieveSquareSudokuNumbers(7, 7);
        }
        else {
          secondStartingGrid = startingGrid.TOPRIGHT;
          startingSquareArray = this.singleSudoku.retrieveSquareSudokuNumbers(7, 1);
        }
        this.secondSudoku = new Sudoku(this.LU, secondStartingGrid, false, startingSquareArray, this.singleSudoku.amountOfRandomNumbers);
        this.secondSudoku.setupGrid();
        this.secondSudoku.generateSudoku();
        break;
      }
      case sudokuType.PYRAMID: {
        //First Sudoku (top section of pyramid):
        this.singleSudoku.setupGrid();
        this.singleSudoku.generateSudoku();
        let startingSquareArray:any = null;

        //Second Sudoku (bottom right section of pyramid):
        let secondStartingGrid:startingGrid = startingGrid.TOPLEFT;
        startingSquareArray = this.singleSudoku.retrieveSquareSudokuNumbers(7, 7);
        this.secondSudoku = new Sudoku(this.LU, secondStartingGrid, false, startingSquareArray, this.singleSudoku.amountOfRandomNumbers);
        this.secondSudoku.setupGrid();
        this.secondSudoku.generateSudoku();

        //Third Sudoku (bottom left section of pyramid):
        let thirdStartingGrid:startingGrid = startingGrid.TOPRIGHT;
        startingSquareArray = this.singleSudoku.retrieveSquareSudokuNumbers(7, 1);
        console.log('third square array: ', startingSquareArray);
        this.thirdSudoku = new Sudoku(this.LU, thirdStartingGrid, false, startingSquareArray, this.singleSudoku.amountOfRandomNumbers);
        this.thirdSudoku.setupGrid();
        this.thirdSudoku.generateSudoku();
        break;
      }
      case sudokuType.SAMURAI: {
        //First Sudoku (Middle Section of Samurai):
        this.singleSudoku.setupGrid();
        this.singleSudoku.generateSudoku();
        let startingSquareArray:any = null;

        //Second Sudoku (bottom right section of pyramid):
        let secondStartingGrid:startingGrid = startingGrid.TOPLEFT;
        startingSquareArray = this.singleSudoku.retrieveSquareSudokuNumbers(7, 7);
        this.secondSudoku = new Sudoku(this.LU, secondStartingGrid, false, startingSquareArray, this.singleSudoku.amountOfRandomNumbers);
        this.secondSudoku.setupGrid();
        this.secondSudoku.generateSudoku();

        //Third Sudoku (bottom left section of pyramid):
        let thirdStartingGrid:startingGrid = startingGrid.TOPRIGHT;
        startingSquareArray = this.singleSudoku.retrieveSquareSudokuNumbers(7, 1);
        this.thirdSudoku = new Sudoku(this.LU, thirdStartingGrid, false, startingSquareArray, this.singleSudoku.amountOfRandomNumbers);
        this.thirdSudoku.setupGrid();
        this.thirdSudoku.generateSudoku();

        //Fourth Sudoku (top right section of pyramid):
        let fourthStartingGrid:startingGrid = startingGrid.BOTTOMLEFT;
        startingSquareArray = this.singleSudoku.retrieveSquareSudokuNumbers(1, 7);
        this.fourthSudoku = new Sudoku(this.LU, fourthStartingGrid, false, startingSquareArray, this.singleSudoku.amountOfRandomNumbers);
        this.fourthSudoku.setupGrid();
        this.fourthSudoku.generateSudoku();

        //Fifth Sudoku (top left section of pyramid):
        let fifthStartingGrid:startingGrid = startingGrid.BOTTOMRIGHT;
        startingSquareArray = this.singleSudoku.retrieveSquareSudokuNumbers(1, 1);
        this.fifthSudoku = new Sudoku(this.LU, fifthStartingGrid, false, startingSquareArray, this.singleSudoku.amountOfRandomNumbers);
        this.fifthSudoku.setupGrid();
        this.fifthSudoku.generateSudoku();
        break;
      }
    }
    this.drawGrid(this.something);

  //  this.drawLettersOnGrid(this.singleSudoku.grid);
  //  this.drawSolutionLettersOnGrid(this.singleSudoku.solutionsGrid);
  }

  isSudokuShapeSelected(x){
    if(x == this.sudokuShapeSelected)
      return true;
    
    return false;
  }

  selectSudokuShape(x){
    switch(x){
      case 1: {
        this.singleSudokuChosen = true;
        break;
      }
      case 2: {
        this.doubleSudokuChosen = true;
        break;
      }
      case 3: {
        this.pyramidSudokuChosen = true;
        break;
      }
      case 4: {
        this.samuraiSudokuChosen = true;
        break;
      }
    }
    this.sudokuShapeSelected = x;
    console.log('the selected sudoku shape is : ', this.sudokuShapeSelected);

  }

  pickRandomStartingGrid(){
    let randomStartingGrid:number = Math.floor(Math.random() * 4);
    switch(randomStartingGrid){
      case 0: {
        this.startingGrid = startingGrid.BOTTOMLEFT;
        console.log('bottomeleft');
        break;
      }
      case 1: {
        this.startingGrid = startingGrid.BOTTOMRIGHT;
        console.log('bottomright');
        break;
      }
      case 2: {
        this.startingGrid = startingGrid.TOPLEFT;
        console.log('topleft');
        break;
      }
      case 3: {
        this.startingGrid = startingGrid.TOPRIGHT;
        console.log('topright');
        break;
      }
      default: {
        break;
      }
    }
  }

  clearSudoku(){
   this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
   this.ctx2.clearRect(0,0, this.canvasSolution.width, this.canvasSolution.height);
   this.grid = [];
   this.setupCanvas();
   this.setupGrid();
  }

  setupCanvas(){
    this.ctx = this.canvas.getContext('2d');
    this.ctx2 = this.canvasSolution.getContext('2d');

    switch(this.something){
      case sudokuType.SINGLE: {
        this.ctx.font = this.ctx2.font = "64px Aria";
        this.ctx.lineWidth = this.ctx2.lineWidth = 15;
        break;
      }
      case sudokuType.DOUBLE: {
    this.ctx.font = this.ctx2.font = "40px Aria";
    this.ctx.lineWidth = this.ctx2.lineWidth = 15;
        break;
      }
      case sudokuType.PYRAMID: {
        this.ctx.font = this.ctx2.font = "35px Aria";
        this.ctx.lineWidth = this.ctx2.lineWidth = 15;
        break;
      }
      case sudokuType.SAMURAI: {
        this.ctx.font = this.ctx2.font = "30px Aria";
        this.ctx.lineWidth = this.ctx2.lineWidth = 15;
        break;
      }
      default: {
        break;
      }
    }
    
  }

  setupGrid(){
    for(let i:number = 0; i < 9; i++)
    {
      this.grid[i] = new Array();
      for(let j:number = 0; j < 9; j++)
      {
        let randomNum:number = Math.floor(Math.random() * 8);
        this.grid[i][j] = new Sudokunumber(0);
      }
    }
  }

  drawGrid(type:sudokuType){
    let paddingY:number = Math.floor(0.1094285 * this.sudokuHeight);
    let paddingX:number = Math.floor(0.0377142 * this.sudokuWidth);
    let originY:number = paddingY;
    let originX:number = paddingX;
    let outerBorderWidth:number = 5;
    let innerBorderWidth:number = 1;

    switch(type){
      case sudokuType.SINGLE: {
        console.log('SINGLE SELECTED');
        let originY:number = paddingY;
        let originX:number = paddingX + 60;
        this.drawOneGrid(5, 1, 900, 900, originX, originY, paddingY);
        this.drawLettersOnGrid(this.singleSudoku.grid, originX, originY, 900);
        this.drawSolutionLettersOnGrid(this.singleSudoku.solutionsGrid, originX, originY, 900);
        break;
      }
      case sudokuType.DOUBLE: {
        console.log('DOUBLE SELECTED');
        let originY:number = paddingY;
        let originX:number = paddingX + 70;
        paddingY = 0.5 * paddingY;
        let sudokuSize:number = 500;

        //Randomize starting grid positions:
        let rand:number = Math.floor(Math.random() * 2);
        let newOriginX:number = originX + (2/3 * sudokuSize);
        let newOriginY:number = originY + (2/3 * sudokuSize);
        if(this.secondSudoku.startGrid == startingGrid.TOPLEFT){
          this.drawOneGrid(4, 1, sudokuSize, sudokuSize, originX, originY, paddingY);
          this.drawOneGrid(4, 1, sudokuSize, sudokuSize, newOriginX, newOriginY, paddingY);
          this.drawLettersOnGrid(this.singleSudoku.grid, originX, originY, sudokuSize);
          this.drawLettersOnGrid(this.secondSudoku.grid, newOriginX, newOriginY, sudokuSize);
          this.drawSolutionLettersOnGrid(this.singleSudoku.solutionsGrid, originX, originY, sudokuSize);
          this.drawSolutionLettersOnGrid(this.secondSudoku.solutionsGrid, newOriginX, newOriginY, sudokuSize);
        } else {
          this.drawOneGrid(4, 1, sudokuSize, sudokuSize, newOriginX, originY, paddingY);
          this.drawOneGrid(4, 1, sudokuSize, sudokuSize, originX, newOriginY, paddingY);
          this.drawLettersOnGrid(this.singleSudoku.grid, newOriginX, originY, sudokuSize);
          this.drawLettersOnGrid(this.secondSudoku.grid, originX, newOriginY, sudokuSize);
          this.drawSolutionLettersOnGrid(this.singleSudoku.solutionsGrid, newOriginX, originY, sudokuSize);
          this.drawSolutionLettersOnGrid(this.secondSudoku.solutionsGrid, originX, newOriginY, sudokuSize);
        }
        break;
      }
      case sudokuType.PYRAMID: {
        console.log('PYRAMID SELECTED');
        let sudokuSize:number = 420;
        let topOriginY:number = paddingY;
        let topOriginX:number = paddingX + ( 2/2.95* sudokuSize);
        let bottomLeftOriginX:number = paddingX + 5;
        let bottomLeftOriginY:number = topOriginY + (2/3 * sudokuSize);
        let bottomRightOriginX:number = bottomLeftOriginX + sudokuSize + ( 1/3 * sudokuSize);
        let bottomRightOriginY:number = bottomLeftOriginY;

        this.drawOneGrid(4, 1, sudokuSize, sudokuSize, topOriginX, topOriginY, paddingY);
        this.drawOneGrid(4, 1, sudokuSize, sudokuSize, bottomLeftOriginX, bottomLeftOriginY, paddingY);
        this.drawOneGrid(4, 1, sudokuSize, sudokuSize, bottomRightOriginX, bottomRightOriginY, paddingY);
        this.drawLettersOnGrid(this.singleSudoku.grid, topOriginX, topOriginY, sudokuSize);
        this.drawLettersOnGrid(this.secondSudoku.grid, bottomRightOriginX, bottomRightOriginY, sudokuSize);
        this.drawLettersOnGrid(this.thirdSudoku.grid, bottomLeftOriginX, bottomLeftOriginY, sudokuSize);
        this.drawSolutionLettersOnGrid(this.singleSudoku.solutionsGrid, topOriginX, topOriginY, sudokuSize);
        this.drawSolutionLettersOnGrid(this.secondSudoku.solutionsGrid, bottomRightOriginX, bottomRightOriginY, sudokuSize);
        this.drawSolutionLettersOnGrid(this.thirdSudoku.solutionsGrid, bottomLeftOriginX, bottomLeftOriginY, sudokuSize);      
        break;
      }
      case sudokuType.SAMURAI: {
        console.log('SAMURAI SELECTED');
        let sudokuSize:number = 380;
        let topLeftOriginX:number = paddingX + 52;
        let topLeftOriginY:number = paddingY;
        let middleOriginX:number = topLeftOriginX + ( 2/3 * sudokuSize);
        let middleOriginY:number = topLeftOriginY + (2/3 * sudokuSize);
        let bottomRightOriginX:number = middleOriginX + (2/3 * sudokuSize);
        let bottomRightOriginY:number = middleOriginY + (2/3 * sudokuSize);
        let topRightOriginX:number = bottomRightOriginX;
        let topRightOriginY:number = topLeftOriginY;
        let bottomLeftOriginX:number = topLeftOriginX;
        let bottomLeftOriginY:number = bottomRightOriginY;
        this.drawOneGrid(4, 1, sudokuSize, sudokuSize, topLeftOriginX, topLeftOriginY, paddingY);
        this.drawOneGrid(4, 1, sudokuSize, sudokuSize, middleOriginX, middleOriginY, paddingY);
        this.drawOneGrid(4, 1, sudokuSize, sudokuSize, bottomRightOriginX, bottomRightOriginY, paddingY);
        this.drawOneGrid(4, 1, sudokuSize, sudokuSize, topRightOriginX, topRightOriginY, paddingY);
        this.drawOneGrid(4, 1, sudokuSize, sudokuSize, bottomLeftOriginX, bottomLeftOriginY, paddingY);
        this.drawLettersOnGrid(this.singleSudoku.grid, middleOriginX, middleOriginY, sudokuSize);
        this.drawLettersOnGrid(this.secondSudoku.grid, bottomRightOriginX, bottomRightOriginY, sudokuSize);
        this.drawLettersOnGrid(this.thirdSudoku.grid, bottomLeftOriginX, bottomLeftOriginY, sudokuSize);
        this.drawLettersOnGrid(this.fourthSudoku.grid, topRightOriginX, topRightOriginY, sudokuSize);
        this.drawLettersOnGrid(this.fifthSudoku.grid, topLeftOriginX, topLeftOriginY, sudokuSize);
        this.drawSolutionLettersOnGrid(this.singleSudoku.solutionsGrid, middleOriginX, middleOriginY, sudokuSize);
        this.drawSolutionLettersOnGrid(this.secondSudoku.solutionsGrid, bottomRightOriginX, bottomRightOriginY, sudokuSize);
        this.drawSolutionLettersOnGrid(this.thirdSudoku.solutionsGrid, bottomLeftOriginX, bottomLeftOriginY, sudokuSize);
        this.drawSolutionLettersOnGrid(this.fourthSudoku.solutionsGrid, topRightOriginX, topRightOriginY, sudokuSize);
        this.drawSolutionLettersOnGrid(this.fifthSudoku.solutionsGrid, topLeftOriginX, topLeftOriginY, sudokuSize);
        break;
      }
      default: {
        break;
      }
    }
  }

  drawOneGrid(outerBorderWidth:number,  innerBorderWidth:number, sudokuWidth:number, sudokuHeight:number, originX:number, originY:number, paddingY:number){
     //Outer Border:
     this.drawLine(originX, originY, originX, originY + sudokuHeight, outerBorderWidth); //Left Wall:
     this.drawLine(originX, originY + sudokuHeight, originX + sudokuWidth, originY + sudokuHeight, outerBorderWidth); //Floor
     this.drawLine(originX + sudokuWidth, originY+ sudokuHeight, originX + sudokuWidth, originY, outerBorderWidth); //Right Wall
     this.drawLine(originX + sudokuWidth, originY, originX, originY, outerBorderWidth); //Roof
 
     // Inner Lines:
     //Vertical:
     this.drawLine(originX + (sudokuWidth / 3), originY, originX + (sudokuWidth / 3), originY + sudokuHeight, outerBorderWidth);
     this.drawLine(originX + (sudokuWidth / 3 * 2), originY, originX + (sudokuWidth / 3 * 2), originY+ sudokuHeight, outerBorderWidth);
     //Horizontal:
     this.drawLine(originX, (originY + (sudokuWidth) / 3), originX + sudokuWidth, originY + (sudokuWidth) / 3, outerBorderWidth);
     this.drawLine(originX, (originY + (sudokuWidth) / 3 * 2), originX + sudokuWidth, originY + (sudokuWidth ) / 3 * 2, outerBorderWidth);
 
     //Inner Inner Lines: 
   //let oneEighth:number = (sudokuWidth - paddingY) / 8;
    let oneEighth:number = (sudokuWidth) / 9;

     this.drawLine(originX + oneEighth,     originY, originX + oneEighth,     originY + sudokuHeight, innerBorderWidth);
     this.drawLine(originX + oneEighth * 2, originY, originX + oneEighth * 2, originY + sudokuHeight, innerBorderWidth);
     this.drawLine(originX + oneEighth * 4, originY, originX + oneEighth * 4, originY + sudokuHeight, innerBorderWidth);
     this.drawLine(originX + oneEighth * 5, originY, originX + oneEighth * 5, originY + sudokuHeight, innerBorderWidth);
     this.drawLine(originX + oneEighth * 7, originY, originX + oneEighth * 7, originY + sudokuHeight, innerBorderWidth);
     this.drawLine(originX + oneEighth * 8, originY, originX + oneEighth * 8, originY + sudokuHeight, innerBorderWidth);
 
     //let oneEighthHeight:number = (sudokuHeight - paddingY) / 9;
     let oneEighthHeight:number = (sudokuHeight) / 9;
     this.drawLine(originX, originY + oneEighthHeight,     originX + sudokuWidth, originY + oneEighthHeight, innerBorderWidth);
     this.drawLine(originX, originY + oneEighthHeight * 2, originX + sudokuWidth, originY + oneEighthHeight * 2, innerBorderWidth);
     this.drawLine(originX, originY + oneEighthHeight * 4, originX + sudokuWidth, originY + oneEighthHeight * 4, innerBorderWidth);
     this.drawLine(originX, originY + oneEighthHeight * 5, originX + sudokuWidth, originY + oneEighthHeight * 5, innerBorderWidth);
     this.drawLine(originX, originY + oneEighthHeight * 7, originX + sudokuWidth, originY + oneEighthHeight * 7, innerBorderWidth);
     this.drawLine(originX, originY + oneEighthHeight * 8, originX + sudokuWidth, originY + oneEighthHeight * 8, innerBorderWidth);
  }

  drawLettersOnGrid(grid, originX, originY, sudokuSize){
    let gridStartX:number = originX + 30;
    let gridStartY:number = originY + 45;
    if(this.something == sudokuType.SINGLE){
       gridStartX = originX + 30;
       gridStartY = originY + 45;
    } else if (this.something == sudokuType.DOUBLE){
      gridStartX = originX + 17;
      gridStartY = originY + 15;
    } else if (this.something == sudokuType.PYRAMID){
      gridStartX = originX + 17;
      gridStartY = originY + 11;
    } else if (this.something == sudokuType.SAMURAI) {
      gridStartX = originX + 13;
      gridStartY = originY + 5;
    }
    for(let i:number = 0; i < 9; i++)
    {
      if(this.something == sudokuType.SINGLE){
        gridStartX = originX + 30;
      } else if(this.something == sudokuType.DOUBLE){
        gridStartX = originX + 17;
      } else if (this.something == sudokuType.PYRAMID){
        gridStartX = originX + 15;
      } else if (this.something == sudokuType.SAMURAI){
        gridStartX = originX + 13;
      }
      for(let j:number = 0; j < 9; j++)
      {
        if(grid[i][j].isHidden == false){
            this.drawLetter(grid[i][j].num, gridStartX, gridStartY, this.ctx);
        }
        gridStartX += sudokuSize / 9;
      }
      gridStartY += sudokuSize / 9;
    }
  }

  drawSolutionLettersOnGrid(grid, originX, originY, sudokuSize){
    let gridStartX:number = originX + 30;
    let gridStartY:number = originY + 45;
    if(this.something == sudokuType.SINGLE){
      gridStartX = originX + 30;
      gridStartY = originY + 45;
    } else if (this.something == sudokuType.DOUBLE){
      gridStartX = originX + 17;
      gridStartY = originY + 15;
    }else if (this.something == sudokuType.PYRAMID){
      gridStartX = originX + 17;
      gridStartY = originY + 11;
    }else if (this.something == sudokuType.SAMURAI) {
      gridStartX = originX + 13;
      gridStartY = originY + 5;
    }
    for(let i:number = 0; i < 9; i++)
    {
      if(this.something == sudokuType.SINGLE){
        gridStartX = originX + 30;
      } else if (this.something == sudokuType.DOUBLE){
        gridStartX = originX + 17;
      }else if (this.something == sudokuType.PYRAMID){
        gridStartX = originX + 15;
      }else if (this.something == sudokuType.SAMURAI){
        gridStartX = originX + 13;
      }
      for(let j:number = 0; j < 9; j++)
      {
        this.drawLetter(grid[i][j].num, gridStartX, gridStartY, this.ctx2);
        gridStartX += sudokuSize / 9;
      }
      gridStartY += sudokuSize / 9;
    }
  }

  drawLetter(char, xPos, yPos, ctx) {
    if(char == '0')
      ctx.fillStyle = "#FF0000";
   ctx.fillText(char, xPos, yPos);

   ctx.fillStyle = "#000";
  }

  drawLine(x0, y0, x1, y1, strokeWidth){
    //Sudoku canvas:
    this.ctx.lineWidth = this.ctx2.lineWidth = strokeWidth;
    this.ctx.beginPath();
    this.ctx.moveTo(x0, y0-25);
    this.ctx.lineTo(x1, y1-25);
    this.ctx.stroke();
    //Solutions canvas:
    this.ctx2.beginPath();
    this.ctx2.moveTo(x0, y0-25);
    this.ctx2.lineTo(x1, y1-25);
    this.ctx2.stroke();
  }
}
   


 //Old working copy of drawing grid
//  let paddingY:number = Math.floor(0.1094285 * this.sudokuHeight);
//  let paddingX:number = Math.floor(0.0377142 * this.sudokuWidth);
//  let originY:number = paddingY;
//  let originX:number = paddingX;
//  let outerBorderWidth:number = 5;
//  let innerBorderWidth:number = 1;

    // //Outer Border:
    // this.drawLine(originX, originY, originX, this.sudokuHeight, outerBorderWidth);
    // this.drawLine(originX, this.sudokuHeight, originX + this.sudokuWidth, this.sudokuHeight, outerBorderWidth);
    // this.drawLine(originX + this.sudokuWidth, this.sudokuHeight, originX + this.sudokuWidth, originY, outerBorderWidth);
    // this.drawLine(originX + this.sudokuWidth, originY, originX, originY, outerBorderWidth);

    // //Inner Lines:
    // this.drawLine(originX + (this.sudokuWidth / 3), originY, originX + (this.sudokuWidth / 3), this.sudokuHeight, outerBorderWidth);
    // this.drawLine(originX + (this.sudokuWidth / 3 * 2), originY, originX + (this.sudokuWidth / 3 * 2), this.sudokuHeight, outerBorderWidth);
    // this.drawLine(originX, (originY + (this.sudokuHeight - paddingY) / 3), originX + this.sudokuWidth, originY + (this.sudokuHeight - paddingY) / 3, outerBorderWidth);
    // this.drawLine(originX, (originY + (this.sudokuHeight - paddingY) / 3 * 2), originX + this.sudokuWidth, originY + (this.sudokuHeight - paddingY) / 3 * 2, outerBorderWidth);

    // //Inner Inner Lines: 
    // let oneEighth:number = (this.sudokuWidth - paddingY) / 8;
    // this.drawLine(originX + oneEighth,     originY, originX + oneEighth,     this.sudokuHeight, innerBorderWidth);
    // this.drawLine(originX + oneEighth * 2, originY, originX + oneEighth * 2, this.sudokuHeight, innerBorderWidth);
    // this.drawLine(originX + oneEighth * 4, originY, originX + oneEighth * 4, this.sudokuHeight, innerBorderWidth);
    // this.drawLine(originX + oneEighth * 5, originY, originX + oneEighth * 5, this.sudokuHeight, innerBorderWidth);
    // this.drawLine(originX + oneEighth * 7, originY, originX + oneEighth * 7, this.sudokuHeight, innerBorderWidth);
    // this.drawLine(originX + oneEighth * 8, originY, originX + oneEighth * 8, this.sudokuHeight, innerBorderWidth);

    // let oneEighthHeight:number = (this.sudokuHeight - paddingY) / 9;
    // this.drawLine(originX, originY + oneEighthHeight,     originX + this.sudokuWidth, originY + oneEighthHeight, innerBorderWidth);
    // this.drawLine(originX, originY + oneEighthHeight * 2, originX + this.sudokuWidth, originY + oneEighthHeight * 2, innerBorderWidth);
    // this.drawLine(originX, originY + oneEighthHeight * 4, originX + this.sudokuWidth, originY + oneEighthHeight * 4, innerBorderWidth);
    // this.drawLine(originX, originY + oneEighthHeight * 5, originX + this.sudokuWidth, originY + oneEighthHeight * 5, innerBorderWidth);
    // this.drawLine(originX, originY + oneEighthHeight * 7, originX + this.sudokuWidth, originY + oneEighthHeight * 7, innerBorderWidth);
    // this.drawLine(originX, originY + oneEighthHeight * 8, originX + this.sudokuWidth, originY + oneEighthHeight * 8, innerBorderWidth);
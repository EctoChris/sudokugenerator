import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LetterUtilService {
  public numbers = [];

  constructor() { 
   this.numbers.push(1,2,3,4,5,6,7,8,9);
  }

  public returnShuffledNumbers(){
    return this.shuffle(this.numbers);
  }

  public checkInvalidRowOrCol(rowOrCol){
    return (new Set(rowOrCol)).size !== rowOrCol.length;
  }

  public returnRandomNumberThatFits(rowArray, colArray, squareArray)
  {
    let newArray = [];
    this.numbers.forEach( function (num ) {
      if(!rowArray.includes(num) && !colArray.includes(num) && !squareArray.includes(num)){
        newArray.push(num);
      }
    });

    newArray = this.shuffle(newArray);
    return newArray[0];
  }

  public returnArrayOfNumbersThatFit(rowArray, colArray, squareArray){
    let newArray = [];
    this.numbers.forEach( function (num ) {
      if(!rowArray.includes(num) && !colArray.includes(num) && !squareArray.includes(num)){
        newArray.push(num);
      }
    });

    newArray = this.shuffle(newArray);
    return newArray;
  }
  public shuffle(array) {
      var currentIndex = array.length, temporaryValue, randomIndex;
  
      // While there remain elements to shuffle...
      while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
     }
    return array;
  }

  public findLastNumberInSquareArray(squareArray)
  {
    let lastNum:number = 0;
    this.numbers.forEach(function(num) {
      if(!squareArray.includes(num)){
        lastNum = num;
      }
    });
    return lastNum;
  }

  public returnRemainingInRowOrCol(rowOrColArray){
    let remaining = [];
    this.numbers.forEach(function (num) {
      // console.log(num);
      if(!rowOrColArray.includes(num))
        remaining.push(num);
    }); 
    return remaining;
  }

  public areArraysEqual(_arr1, _arr2){
      if (!Array.isArray(_arr1) || ! Array.isArray(_arr2) || _arr1.length !== _arr2.length)
        return false;

      var arr1 = _arr1.concat().sort();
      var arr2 = _arr2.concat().sort();

      for (var i = 0; i < arr1.length; i++) {

          if (arr1[i] !== arr2[i])
              return false;

      }

      return true;
  }

  public returnPotentialNumbers(squareArray, remainingInRow){
    let potentialNumbers = [];
    remainingInRow.forEach(function (num){
      if(!squareArray.includes(num))
        potentialNumbers.push(num);
    });
    return potentialNumbers;
  }

  public returnBusiestRowOrCol(array){
    // let max:number = Math.max(array) - 1;
    let max:number = 0;
    let pos:number;
    for(let i:number = 0; i < array.length; i++)
    {
        if(array[i] > max)
        {
          max = array[i];
          pos = i;
        }
    }
    let maxNumAndPosition = [max, pos ];
    return maxNumAndPosition;
  }

  public orderRemainingNumbers(numbersLeft, arrayOne, arrayTwo, arrayThree){
    let tempArray = numbersLeft;
    let tempNum:number = null;
    let successfulMatch:boolean = false;
    for(let i:number = 0; i < 6; i++)
    {
      successfulMatch = this.checkNumbers(tempArray, arrayOne, arrayTwo, arrayThree);
      if(successfulMatch)
        return tempArray;

      if(i % 2 == 0) //even
      { 
        //Swap 2 + 1
        tempNum = tempArray[2];
        tempArray[2] = tempArray[1];
        tempArray[1] = tempNum;
      } else {
        //Swap 1 + 0
        tempNum = tempArray[1];
        tempArray[1] = tempArray[0];
        tempArray[0] = tempNum;
      }
    }
    
    let array = [];
    return array;
  }
  public returnMissingNumbersFromRowAndCol(rowArray, colArray){
    let newArray = [];
    this.numbers.forEach(function (num) {
      if(!rowArray.includes(num) && !colArray.includes(num))
      {
        newArray.push(num);
      }
    });
    return newArray;
  }

  public returnPotentialOuterNumbers(squareArray, colOrRowArray){
    let newArray = [];
    colOrRowArray.forEach(function(num) {
      if(!squareArray.includes(num))
        newArray.push(num);
    });
    return newArray;
  }

  public removeZeros(array)
  {
    let newArray = [];
    for( let i:number = 0; i < array.length; i++)
    {
      if(array[i] != 0)
        newArray.push(array[i]);
    }
    return newArray;
  }
  public checkNumbers(testArray, arrayOne, arrayTwo, arrayThree){
    if(arrayOne.includes(testArray[0]))
      return false;
    if(arrayTwo.includes(testArray[1]))
      return false;
    if(arrayThree.includes(testArray[2]))
      return false;
    
    return true;
  }

  public fetchRowOrCol(rowOrCol){
    let startAndEnd = [];
    switch(rowOrCol){
      case 0:
      case 1:
      case 2:
      {
        startAndEnd.push(0, 3);
        break;
      }
      case 3:
      case 4:
      case 5:
      {
        startAndEnd.push(3, 6);
        break;
      }
      case 7:
      case 8:
      case 9:
      {
        startAndEnd.push(6, 9);
        break;
      }
    }
    return startAndEnd;
  }


}

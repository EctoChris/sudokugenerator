import { TestBed } from '@angular/core/testing';

import { LetterUtilService } from './letter-util.service';

describe('LetterUtilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LetterUtilService = TestBed.get(LetterUtilService);
    expect(service).toBeTruthy();
  });
});

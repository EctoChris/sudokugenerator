// showSudokuNumbers(){
//     //Select random number between 19 & 36:
//     let overallNumbersToShow:number = Math.floor(Math.random() * (36-19))  + 19;

//     //Select numbers to be shown p/Row:
//     let rowNumbersToShow = this.splitIntoThree(overallNumbersToShow);

//     //Select Inner Row Values:
//     let firstInnerRow = this.splitIntoThreeInner(rowNumbersToShow[0]);
//     let middleInnerRow = this.splitIntoThree(rowNumbersToShow[1]);
//     let finalInnerRow = [];
//     finalInnerRow.push(firstInnerRow[2], firstInnerRow[1], firstInnerRow[0]);

//     console.log('overallNumbersToShow:');
//     console.log(overallNumbersToShow);

//     console.log('Rows:');
//     console.log(rowNumbersToShow);
//     //Numbers to be shown:
//     console.log('Numbers to be shown:');
//     console.log(firstInnerRow);
//     console.log(middleInnerRow);
//     console.log(finalInnerRow);

//     //Show numbers on firstRow:
//     for(let j:number = 0; j < firstInnerRow.length; j++)
//     {
//       for(let i:number = 0; i < firstInnerRow[j]; i++)
//       {
//         let currentSquareArray = this.retrieveSquareArray(0, j * 4, true, false);
//         let randomNum = currentSquareArray[Math.floor(Math.random() * currentSquareArray.length)];
//         this.showNumber(0, j * 4, randomNum);
//       }
//     }

//     //Show numbers on secondRow[0]:
//     for(let i:number = 0; i < middleInnerRow[0]; i++)
//     {
//       let currentSquareArray = this.retrieveSquareArray(4, 1, true, false);
//       let randomNum = currentSquareArray[Math.floor(Math.random() * currentSquareArray.length)];
//       this.showNumber(4, 1, randomNum);
//     }

//     //Show Numbers in Center Square:
//     let centerNumbersCount:number = middleInnerRow[1];
//     console.log('centerNumbersCount = ' + middleInnerRow[1]);

//     switch(centerNumbersCount){
//       case 1:{
//         this.showNumberByPos(1,1);
//         break;
//       }
//       case 2:
//       case 3:{
//         this.twoOrThreeNums(centerNumbersCount);
//         break;
//       }
//       case 4:
//       case 5:{
//         this.fourOrFiveNums(centerNumbersCount);
//         break;
//       }
//       case 6: 
//       case 7: {
//         this.sixOrSevenNums(centerNumbersCount);
//         break;
//       }
//       default: {
//         console.log('default');
//         break;
//       }
//     }

//     //Mirror secondRow[0]:
//     let secondRow0Mirrored = this.getMirrorSquareArray(4, 1);

//     //Mirror each square in firstRow for finalRow:
//     let firstRow0Mirrored = this.getMirrorSquareArray(0, 0);
//     let firstRow1Mirrored = this.getMirrorSquareArray(0, 4);
//     let firstRow2Mirrored = this.getMirrorSquareArray(0, 8);
//     this.revealSquareArray(8, 8, firstRow0Mirrored);
//     this.revealSquareArray(8, 4, firstRow1Mirrored);
//     this.revealSquareArray(8, 0, firstRow2Mirrored);
//     this.revealSquareArray(4, 8, secondRow0Mirrored);
//   }


// twoOrThreeNums(num){
//     let randomCol:number = Math.round(Math.random() * 3);
//     console.log(randomCol + ' is randomCol, it is 2 or 3');
//     if(randomCol == 1){
//       //Place on [0, 1], [2, 1]
//       this.showNumberByPos(0,1);
//       this.showNumberByPos(2,1);
//     }
//     else{
//       //Pick random on col[0]
//       let random:number = Math.round(Math.random() * 2);
//       this.showNumberByPos(random, 0);

//       //Mirror on col[2]
//       this.showNumberByPos(this.mirrorSpecificNum(random), 2);

//     }

//     if(num == 3){
//       //Place on [1, 1]
//       this.showNumberByPos(1,1);
//     }
//   }
//   mirrorSpecificNum(num){
//     if(num == 2){
//       num = 0;
//     }
//     else if(num == 0){
//       num = 2;
//     }
//     return num;
//   }
//   fourOrFiveNums(num){
//     let randomNum:number = Math.round(Math.random() * 5);
//     switch(randomNum){
//       case 0:{
//         //Place on [1,0], [2,0], [0,2], [1,2]:
//         this.showNumberByPos(1, 0);
//         this.showNumberByPos(2, 0);
//         this.showNumberByPos(0, 2);
//         this.showNumberByPos(1, 2);
//         console.log('CASE 0:');
//         break;
//       }
//       case 1:{
//         //Place on [0,0], [1,0], [1,2], [2,2]:
//         this.showNumberByPos(0,0);
//         this.showNumberByPos(1,0);
//         this.showNumberByPos(1,2);
//         this.showNumberByPos(2,2);
//         console.log('CASE 1:');
//         break;
//       }
//       case 2: {
//         //Place on [0,0], [0,2], [2,0], [2,2]:
//         this.showNumberByPos(0,0);
//         this.showNumberByPos(0,2);
//         this.showNumberByPos(2,0);
//         this.showNumberByPos(2,2);
//         console.log('CASE 2:');
//         break;
//       }
//       case 3: {
//         //Place on [1,0], [0,1], [1,2], [2,1]:
//         this.showNumberByPos(1,0);
//         this.showNumberByPos(0,1);
//         this.showNumberByPos(1,2);
//         this.showNumberByPos(2,1);
//         console.log('CASE 3:');
//         break;
//       }
//       case 4: {
//         //Place on [0,0], [0,1], [2,1], [2,2]:
//         this.showNumberByPos(0,0);
//         this.showNumberByPos(0,1);
//         this.showNumberByPos(2,1);
//         this.showNumberByPos(2,2);
//         console.log('CASE 4:');
//         break;
//       }
//       case 5: {
//         //Place on [2,0], [2,1], [0,1], [0,2]:
//         this.showNumberByPos(2,0);
//         this.showNumberByPos(2,1);
//         this.showNumberByPos(0,1);
//         this.showNumberByPos(0,2);
//         console.log('CASE 5:');
//         break;
//       }
//       default: {
//         break;
//       }
//     }

//     if(num == 5){
//       //Place on [1,1]:
//       this.showNumberByPos(1,1);
//     }
//   }

//   //Make method to place on sudoku + mirror its equivelant:

//   sixOrSevenNums(num){
//     let randomNum:number = Math.round(Math.random() * 3);
//     switch(randomNum){
//       case 0: {
//         //Place on [0,0], [1,0], [2,0], [0,2], [1,2], [2,2]:
//         this.showNumberByPos(0,0);
//         this.showNumberByPos(1,0);
//         this.showNumberByPos(2,0);
//         this.showNumberByPos(0,2);
//         this.showNumberByPos(1,2);
//         this.showNumberByPos(2,2);
//         break;
//       }
//       case 1: {
//         //Place on [0,0], [0,1], [0,2], [2,0], [2,1], [2,2]:
//         this.showNumberByPos(0,0);
//         this.showNumberByPos(0,1);
//         this.showNumberByPos(0,2);
//         this.showNumberByPos(2,0);
//         this.showNumberByPos(2,1);
//         this.showNumberByPos(2,2);
//         break;
//       }
//       case 2: {
//         //Place on [0,0], [0,1], [1,0], [1,2], [2,1], [2,2]:
//         this.showNumberByPos(0,0);
//         this.showNumberByPos(0,1);
//         this.showNumberByPos(1,0);
//         this.showNumberByPos(1,2);
//         this.showNumberByPos(2,1);
//         this.showNumberByPos(2,2);
//         break;
//       }
//       case 3: {
//         //Place on [1,0], [2,0], [0,1], [2,1], [0,2], [1,2]:
//         this.showNumberByPos(1,0);
//         this.showNumberByPos(2,0);
//         this.showNumberByPos(0,1);
//         this.showNumberByPos(2,1);
//         this.showNumberByPos(0,2);
//         this.showNumberByPos(1,2);
//         break;
//       }
//       default: {
//         break;
//       }
//     }
//     if(num == 7) {
//       //Place on [1,1]
//       this.showNumberByPos(1,1);
//     }
//   }

// getMirrorSquareArray(row, col){
//     let squareArray2D:any =[];
//     let squareArray:any = this.retrieveSquareArray(row, col, false, true);
//     let firstRow = [squareArray[0], squareArray[1], squareArray[2]];
//     let secondRow = [squareArray[3], squareArray[4], squareArray[5]];
//     let thirdRow = [squareArray[6], squareArray[7], squareArray[8]];
//     squareArray2D.push(firstRow, secondRow, thirdRow);

//     //Create empty mirror array:
//     let mirrorArray:any = [];
//     for(let i:number = 0; i < 3; i++)
//     {
//       mirrorArray[i] = new Array();
//       for (let j:number = 0; j < 3; j++)
//       {
//         mirrorArray[i][j] = null;
//       }
//     }

//     for(let i:number = 0; i < 3; i++)
//     {
//       for(let j:number = 0; j < 3; j++)
//       {
//         if(squareArray2D[i][j] != null)
//         {
//           let mirrorRow = i;
//           let mirrorCol = j;
//           if(i == 0)
//             mirrorRow = 2;
//           if(j == 0)
//             mirrorCol = 2;
//           if(i == 2)
//             mirrorRow = 0;
//           if(j == 2)
//             mirrorCol = 0;
          
//           mirrorArray[mirrorRow][mirrorCol] = squareArray2D[i][j];
//         }
//       }
//     }

//     return mirrorArray;
//   }

// splitIntoThree(num){
//     let rowOrColNumbers:any = [];

//     //Pick number to appear twice:
//     let percentage:number = Math.random() * (36.1 - 28.1) + 28.1;
//     let commonNumber:number = Math.round(percentage * num/100);

//     // rowOrColNumbers.push(commonNumber, commonNumber);
//     let remainder:number = num - (commonNumber * 2);
//     rowOrColNumbers.push(commonNumber, remainder, commonNumber);

//     return rowOrColNumbers;
//   } 

//   splitIntoThreeInner(num){
//     let rowOrColNumbers:any = [];
//     let tempArray:any = [];

//     let percentage:number = Math.random() * (36.1 - 16.7) + 16.7;
//     let percentage2:number = Math.random() * (36.1 - 16.7) + 16.7;

//     let firstNumber:number = Math.round(percentage * num/100);
//     let secondNumber:number = Math.round(percentage2 * num/100);
//     let remainder:number = num - firstNumber - secondNumber;

//     tempArray.push(firstNumber, secondNumber, remainder);
    
//     //Find highest number:
//     let highestNum:number = 0;
//     let highestNumPos:number = null;
//     for(let i:number = 0; i < 3; i++)
//     {
//       if(tempArray[i] > highestNum)
//       {
//         highestNum = tempArray[i];
//         highestNumPos = i;
//       }
//     }

//     //Remove highest number:
//     tempArray.splice(highestNumPos, 1);

//     rowOrColNumbers.push(tempArray[0], highestNum, tempArray[1]);
//     return rowOrColNumbers;
//   }
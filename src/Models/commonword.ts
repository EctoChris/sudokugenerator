export class CommonWord {
    public word:string;
    public definition:string;

    constructor(word, definition){
        this.word = word;
        this.definition = definition;
    }
}
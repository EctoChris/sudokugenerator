import { LetterUtilService } from 'src/app/letter-util.service';
import { Sudokunumber } from '../Models/sudokunumber';
import * as _ from 'lodash';
import { IfStmt } from '@angular/compiler';

enum startingGrid {
    TOPRIGHT = "TOPRIGHT",
    BOTTOMRIGHT = "BOTTOMRIGHT",
    BOTTOMLEFT = "BOTTOMLEFT",
    TOPLEFT = "TOPLEFT"
  }

export class Sudoku {
    public rows =    [0,1,2,3,4,5,6,7,8];
    public columns = [0,1,2,3,4,5,6,7,8];
    public sudokuHeight:number = 700;
    public sudokuWidth:number = 700;
    public grid:any = [];
    public tempGrid:any = [];
    public solutionsGrid:any = [];
    public lastCoordRemoved = [];
    public solutionsCount:number = 0;
    public lastNumRemoved:number = 0;
    public isFirstSudoku:boolean = false;
    public startGrid:startingGrid;
    public firstSquareArray:any;
    public tempStop:boolean = false;
    public amountOfRandomNumbers:number = 0;
    public gridCounts;

    constructor(private LU: LetterUtilService, startGrid:startingGrid,  isFirstSudoku:boolean, firstSquareArray:any, initialNumbers:number){
        this.isFirstSudoku = isFirstSudoku;
        this.startGrid = startGrid;
        this.firstSquareArray = firstSquareArray;
        this.amountOfRandomNumbers = initialNumbers;
    }

    copyStartGrid(startRow, endRow, startCol, endCol, firstSquareArray){
      let counter:number = 0;
      for(let i:number = startRow; i < endRow; i++)
      {
        for( let j:number = startCol; j < endCol; j++)
        {
           this.grid[i][j].num = firstSquareArray[counter].num;
           this.grid[i][j].isHidden = firstSquareArray[counter].isHidden;
           //this.grid[i][j].isHidden = firstsquar
           counter++;
        }
      }
    }

    setupGrid(){
        for(let i:number = 0; i < 9; i++)
        {
          this.grid[i] = new Array();
          for(let j:number = 0; j < 9; j++)
          {
            this.grid[i][j] = new Sudokunumber(0);
          }
        }
    }

    generateSudoku(){
        let invalidSudoku:boolean = true;
        while(invalidSudoku)
        {
           //Randomize starting grid:
          if(this.isFirstSudoku == true){
            this.pickRandomStartingGrid();
          } 
          this.clearSudoku();
          this.buildSudoku(this.startGrid);
          invalidSudoku = this.checkSudoku();
        }

        console.log('invalid sudoku is  ', invalidSudoku);
        //Save solutions before hiding numbers:
        this.solutionsGrid = _.cloneDeep(this.grid);    
        let count:number = 100;
        
        //Pick initial number of numbers to be shown (between 27 - 40):
        if(this.isFirstSudoku){
          this.amountOfRandomNumbers = Math.floor(Math.random() * (38 - 27) + 27);
        }
        console.log('AMOUNTOFRANDOMNUMBERS:' + this.amountOfRandomNumbers);
        while(count > this.amountOfRandomNumbers){
          this.hideNumbers();
          count = this.countVisibleNumbers();
          console.log('numbers shown: ' ,count);
        }
        console.log('FINISHED A SUDOKU' );
        console.log('FINISHED A SUDOKU' );      
      }

    //Hide numbers:
    hideNumbers(){
      let uniqueSolution:boolean = true;
      let count:number = 100;
        while(uniqueSolution && count > this.amountOfRandomNumbers){

          if(this.isFirstSudoku == true){
            //Remove a number:
            this.removeNumber();
          } else {
            this.removeNumberWithRestrictedSquare();
          }

          //Check if sudoku solution still unique:
          this.solutionsCount = 0;
          this.solveGame();

          count = this.countVisibleNumbers();
          console.log('count = ',count);
          if(this.solutionsCount != 1){
            //not valid anymore:
            uniqueSolution = false;
          }
        }

        if(uniqueSolution == false){
          //Re-add last removed number:
          this.grid[this.lastCoordRemoved[0]][this.lastCoordRemoved[1]].num = this.lastNumRemoved;
          this.grid[this.lastCoordRemoved[0]][this.lastCoordRemoved[1]].isHidden = false;
          this.solutionsCount = 0;
        }
      }

      countGrid(row, col){
        let rowStart:number
        let rowEnd:number;
        let colStart:number;
        let colEnd:number;
        switch(row){
          case 0: case 1: case 2: {
            rowStart = 0;
            rowEnd = 3;
            break;
          }
          case 3: case 4: case 5:{
            rowStart = 3;
            rowEnd = 6;
            break;
          }
          case 6: case 7: case 8: {
            rowStart = 6;
            rowEnd = 9;
            break;
          }
        }
        console.log('rowStart: '+ rowStart);
        console.log('rowEnd: ', rowEnd);
        switch(col){
          case 0: case 1: case 2: {
            colStart = 0;
            colEnd = 3;
            break;
          }
          case 3: case 4: case 5: {
            colStart = 3;
            colEnd = 6;
            break;
          }
          case 6: case 7: case 8: {
            colStart = 6;
            colEnd = 9;
            break;
          }
        }
        let count:number = 0;

        for(let i:number = rowStart; i < rowEnd; i++)
        {
          for(let j:number = colStart; j < colEnd; j++)
          {
            if(this.grid[i][j].num != 0){
              count++;
            }
          }
        }
        return count;
      }

      removeNumber(){
        let numberWasRemoved:boolean = false;
        while(numberWasRemoved == false){
          // Pick random coord:
          let row:number = Math.floor(Math.random() * 9);
          let col:number = Math.floor(Math.random() * 9);
          
          //OPTIONAL CONDITION: have a minimum of 2 numbers in each grid:
          let count = this.countGrid(row, col);

          //if(this.grid[row][col].num != 0)
          if(this.grid[row][col].num != 0 && count > 2)
          {
            //Save Last Number Removed:
            this.lastCoordRemoved = [];
            this.lastCoordRemoved.push(row);
            this.lastCoordRemoved.push(col);
            this.lastNumRemoved = this.grid[row][col].num;
            //Remove Number:
            this.grid[row][col].num = 0;
            numberWasRemoved = true;
          }
        }
      }

      removeNumberWithRestrictedSquare(){
        let numberWasRemoved:boolean = false;
        while(numberWasRemoved == false){
          //Pick random coord:
          let row:number = Math.floor(Math.random() * 9);
          let col:number = Math.floor(Math.random() * 9);

          //Check if coord is within restricted square:
          let inRestrictedSquare:boolean = true;
          inRestrictedSquare = this.isCoordInStartGrid(row, col);

          //OPTIONAL CONDITION: have a minimum of 2 numbers in each grid:
          let count = this.countGrid(row, col);
          
          //if(inRestrictedSquare == false && this.grid[row][col].num != 0)
          if(inRestrictedSquare == false && this.grid[row][col].num != 0 && count > 2){
              //Save Last Number Removed:
              this.lastCoordRemoved = [];
              this.lastCoordRemoved.push(row);
              this.lastCoordRemoved.push(col);
              this.lastNumRemoved = this.grid[row][col].num;
              this.grid[row][col].num = 0;
              numberWasRemoved = true;
          }
        }
      }

      resetNumbersToVisible(){
        for(let i:number = 0; i < 9; i++)
        {
          for(let j:number = 0; j < 9; j++)
          {
            this.grid[i][j].isHidden = false;
          }
        }
      }
      
      countVisibleNumbers(){
        let count:number = 0;
        for(let i:number = 0; i < 9; i ++)
        {
          for(let j:number = 0; j < 9; j++)
          {
             if(this.grid[i][j].isHidden == false){
                count++;
             }
          }
        }
        return count;
      }
    
      checkSudoku(){
        let invalidSudoku:boolean = false;
        for(let i:number = 0; i < 9; i++)
        {
          for(let j:number = 0; j < 9; j++)
          {
            if(this.grid[i][j].num == null || this.grid[i][j].num == 0)
            {
              invalidSudoku = true;
            }
          }
        }
    
        for(let i:number = 0; i < 8; i++)
        {
          let row = this.grabRow(i);
          let col = this.grabCol(i);
    
          if(this.LU.checkInvalidRowOrCol(row))
            return true;
          
          if(this.LU.checkInvalidRowOrCol(col)){
            return true;
          }
        }
        return invalidSudoku;
      }

      pickRandomStartingGrid(){
        let randomStartingGrid:number = Math.floor(Math.random() * 4);
        switch(randomStartingGrid){
          case 0: {
            this.startGrid = startingGrid.BOTTOMLEFT;
            console.log('bottomeleft');
            break;
          }
          case 1: {
            this.startGrid = startingGrid.BOTTOMRIGHT;
            console.log('bottomright');
            break;
          }
          case 2: {
            this.startGrid = startingGrid.TOPLEFT;
            console.log('topleft');
            break;
          }
          case 3: {
            this.startGrid = startingGrid.TOPRIGHT;
            console.log('topright');
            break;
          }
          default: {
            break;
          }
        }
      }

      clearSudoku(){
        this.grid = [];
        this.setupGrid();
      }

      buildSudoku(startPos){
        let startRow:number = 0;
        let startCol:number = 0;
        let endRow:number = 3;
        let endCol:number = 3;
        let isTop:boolean = true;
        let isLeft:boolean = true;
        switch(startPos)
        {
          case startingGrid.TOPLEFT:{
            this.fillSquares(startRow, endRow, startCol, endCol, isTop, isLeft);
            break;
          }
          case startingGrid.TOPRIGHT: {
            isLeft = false;
            startCol = 6;
            endCol = 9;
            this.fillSquares(startRow, endRow, startCol, endCol, isTop, isLeft);
            break;
          }
          case startingGrid.BOTTOMRIGHT: {
            isLeft = false;
            startCol = 6;
            endCol = 9;
            isTop = false;
            startRow = 6;
            endRow = 9;
            this.fillSquares(startRow, endRow, startCol, endCol, isTop, isLeft);
            break;
          }
          case startingGrid.BOTTOMLEFT: {
            startRow = 6;
            endRow = 9;
            isTop = false;
            this.fillSquares(startRow, endRow, startCol, endCol, isTop, isLeft);
            break;
          }
          default: 
          {
            break;
          }
        }
      }
    
      fillSquares(startRow, endRow, startCol, endCol, isTop, isLeft){
        if(this.isFirstSudoku){
          this.fillFirstSquare(startRow, endRow, startCol, endCol, isTop);
        } 
        else 
        {
          if(this.startGrid == startingGrid.TOPRIGHT){
            this.copyStartGrid(0, 3, 6, 9, this.firstSquareArray);
          }
          else if (this.startGrid == startingGrid.TOPLEFT){
            this.copyStartGrid(0, 3, 0, 3, this.firstSquareArray);
          } 
          else if (this.startGrid == startingGrid.BOTTOMRIGHT){
            this.copyStartGrid(6, 9, 6, 9, this.firstSquareArray);
          } 
          else if (this.startGrid == startingGrid.BOTTOMLEFT){
            this.copyStartGrid(6, 9, 0, 3, this.firstSquareArray);
          } else {
            console.log('no conditions hit');
          }
        }

        this.fillSecondSquare(startRow, endRow, startCol, endCol, isTop);
        this.fillThirdSquare(isTop, isLeft);
        this.fillFourthSquare(startRow, endRow, startCol, endCol, isLeft);
        this.fillFifthSquare(isTop, isLeft);
        this.newFillMiddleSquare(isTop, isLeft);
        this.fillSeventhSquare(isTop, isLeft);
        this.fillEigthSquare(isTop, isLeft);
        this.fillFinalSquare(isTop, isLeft);
      }


      fillFirstSquare(startRow, endRow, startCol, endCol, isTop){
        //First Square:
        let shuffledNumbers = this.LU.returnShuffledNumbers();
        console.log('shuffled numbers: ' , shuffledNumbers);
        let counter:number = 0;
        for(let i:number = startRow; i < endRow; i++)
        {
            for(let j:number = startCol; j < endCol; j++)
            {
               this.grid[i][j].num = shuffledNumbers[counter];
               counter++;
            }
        }
      }
    
      fillSecondSquare(startRow, endRow, startCol, endCol, isTop){
        //Second Square:
        let sampleCell = [0, 4];
        let rows = [0, 1, 2];
        if(!isTop)
        {
          rows = [6, 7, 8];
          sampleCell =  [8, 4]
        }
    
        //Row 0:
        let remainingInRow = this.findRemainingInRow(rows[0]);
        let shuffledRemaining = this.LU.shuffle(remainingInRow); //Pick 3 random
        let counter:number = 0;
        for(let i:number = 3; i < 6; i++)
        {
          this.grid[rows[0]][i].num = shuffledRemaining[counter];
          counter++;
        }
        
        //Row 1:
        let squareArray = this.retrieveSquareArray(sampleCell[0], sampleCell[1], false, false);    //Pick from remaining row 2 first, if space left, pick from rows 0 & 1
        let lastRowGenerated = this.retrieveLastRow(endRow, startCol, endCol);
        let remainingRow1 = this.LU.returnPotentialNumbers(squareArray, lastRowGenerated);
        if(remainingRow1.length < 3)
        {
          while(remainingRow1.length < 3)
          {
              let firstRow = this.LU.shuffle(this.retrieveFirstRow(startRow, startCol, endCol));
              let potentialNumbers = this.LU.returnPotentialNumbers(squareArray, firstRow);
              if(!remainingRow1.includes(potentialNumbers[0]))
                remainingRow1.push(potentialNumbers[0]);
          }
        }
        counter = 0;
        for(let i:number = 3; i < 6; i++)
        {
          this.grid[rows[1]][i].num = remainingRow1[counter];
          counter++;
        }
    
        //Row 2:
        squareArray = this.retrieveSquareArray(sampleCell[0], sampleCell[1], false, false);
        remainingInRow = this.findRemainingInRow(rows[2]);
        let remainingRow2 = this.LU.returnPotentialNumbers(squareArray, remainingInRow);
        counter = 0;
        for(let i:number = 3; i < 6; i++)
        {
          this.grid[rows[2]][i].num = remainingRow2[counter];
          counter++;
        }
      }
    
      fillFourthSquare(startRow, endRow, startCol, endCol, isLeft){
        let sampleCell = [4, 1];
        let cols = [0, 1, 2];
        let rows = [3, 6];
        if(!isLeft)
        {
          sampleCell = [5, 7];
          cols = [6, 7, 8];
        }
        
        //Col 0:
        let remainingInCol = this.findRemainingInCol(cols[0]);
        let shuffledRemaining = this.LU.shuffle(remainingInCol); //Pick 3 random
        let counter:number = 0;
        for(let i:number = rows[0]; i < rows[1]; i++)
        {
          this.grid[i][cols[0]].num = shuffledRemaining[counter];
          counter++;
        }
    
        //Col 1:
        let squareArray = this.retrieveSquareArray(sampleCell[0], sampleCell[1], false, false);
    
        //Pick from remaining col 2 first, if space left, pick from rows 0 & 1
        let lastColGenerated = this.retrieveLastCol(cols[2], startRow, endRow);
    
        //If not in current Sudoku Square: Add to list:
        let potentialNumbers = this.LU.shuffle(this.LU.returnPotentialNumbers(squareArray, lastColGenerated));
    
        if(potentialNumbers.length < 3)
        {
          while(potentialNumbers.length < 3)
          {
            //If space left, pick from col 0:
            let firstColGenerated = this.LU.shuffle(this.retrieveLastCol(cols[0], startRow, endRow));
            if(!potentialNumbers.includes(firstColGenerated[0]))
              potentialNumbers.push(firstColGenerated[0]);
          }
        }
    
        counter = 0;
        for(let i:number = rows[0]; i< rows[1]; i++)
        {
          this.grid[i][cols[1]].num = potentialNumbers[counter];
          counter++;
        }
    
        //Col 2:
        squareArray = this.retrieveSquareArray(sampleCell[0], sampleCell[1], false, false);
        remainingInCol = this.findRemainingInCol(cols[2]);
        let remainingInCol2 = this.LU.returnPotentialNumbers(squareArray, remainingInCol);
    
        counter = 0;
        for(let i:number = rows[0]; i < rows[1]; i++)
        {
          this.grid[i][cols[2]].num = remainingInCol2[counter];
          counter++;
        }
      }
    
      fillThirdSquare(isTop, isLeft){
        let rows = [0, 1, 2];
        let cols = [6, 9];
    
        if(!isTop){
          rows = [6, 7, 8];
        }
        if(!isLeft){
          cols = [0, 3];
        }
    
        //Third Square:
        for(let j:number = 0; j < 3; j++)
        {
          let remainingInRow = this.LU.shuffle(this.findRemainingInRow(rows[j]));
          let counter:number = 0;
          for(let i:number = cols[0]; i < cols[1]; i++)
          {
            this.grid[rows[j]][i].num = remainingInRow[counter];
            counter++;
          }
        }
      }
    
      reshuffleThirdSquare(isTop, isLeft){
        let rows = [0, 1, 2];
        let cols = [6, 9];
    
        if(!isTop){
          rows = [6, 7, 8];
        }
        if(!isLeft){
          cols = [0, 3];
        }
    
        let shuffledNumbers = [];
        //Grab last 3 elements from col arrays:
        let grabbedRow0 = this.grabRow(rows[0]);
        let thirdSquareRow0 = grabbedRow0.slice(Math.max(grabbedRow0.length - 3, 0));
    
        let grabbedRow1 = this.grabRow(rows[1]);
        let thirdSquareRow1 = grabbedRow1.slice(Math.max(grabbedRow1.length - 3, 0));
    
        let grabbedRow2 = this.grabRow(rows[2]);
        let thirdSquareRow2 = grabbedRow2.slice(Math.max(grabbedRow2.length - 3, 0));
    
        thirdSquareRow0 = this.LU.shuffle(thirdSquareRow0);
        thirdSquareRow1 = this.LU.shuffle(thirdSquareRow1);
        thirdSquareRow2 = this.LU.shuffle(thirdSquareRow2);
        shuffledNumbers.push(thirdSquareRow0);
        shuffledNumbers.push(thirdSquareRow1);
        shuffledNumbers.push(thirdSquareRow2);
    
         //Third Square:
         for(let j:number = 0; j < 3; j++)
         {
           let counter:number = 0;
           for(let i:number = cols[0]; i < cols[1]; i++)
           {
             this.grid[rows[j]][i].num = shuffledNumbers[j][counter];
             counter++;
           }
         }
      }
    
      fillFifthSquare(isTop, isLeft){
        let rows = [6, 9];
        let cols = [0, 1, 2];
        if(!isTop){
           rows = [0, 3];
        }
        if(!isLeft){
          cols = [6, 7, 8];
        }
    
        //Fifth Square: 
        //Col 0:
        for(let j:number = 0; j < 3; j++)
        {
          let remainingInCol = this.findRemainingInCol(cols[j]);
          let shuffledRemaining = this.LU.shuffle(remainingInCol); //Pick 3 random
          let counter:number = 0;
          for(let i:number = rows[0]; i < rows[1]; i++)
          {
            this.grid[i][cols[j]].num = shuffledRemaining[counter];
            counter++;
          }
        }
      }
    
      reshuffleFifthSquare(isTop, isLeft){
        let rows = [6, 9];
        let cols = [0, 1, 2];
        if(!isTop){
           rows = [0, 3];
        }
        if(!isLeft){
          cols = [6, 7, 8];
        }
    
        let shuffledNumbers = [];
        //Grab last 3 elements from col arrays:
        let grabbedCol0 = this.grabCol(cols[0]);
        let fifthSquareCol0 = grabbedCol0.slice(Math.max(grabbedCol0.length - 3, 0));
    
        let grabbedCol1 = this.grabCol(cols[1]);
        let fifthSquareCol1 = grabbedCol1.slice(Math.max(grabbedCol1.length - 3, 0));
    
        let grabbedCol2 = this.grabCol(cols[2]);
        let fifthSquareCol2 = grabbedCol2.slice(Math.max(grabbedCol2.length - 3, 0));
    
    
        fifthSquareCol0 = this.LU.shuffle(fifthSquareCol0);
        fifthSquareCol1 = this.LU.shuffle(fifthSquareCol1);
        fifthSquareCol2 = this.LU.shuffle(fifthSquareCol2);
        shuffledNumbers.push(fifthSquareCol0);
        shuffledNumbers.push(fifthSquareCol1);
        shuffledNumbers.push(fifthSquareCol2);
    
        for(let j:number = 0; j < 3; j++)
        {
          let counter:number = 0;
          for(let i:number = rows[0]; i < rows[1]; i++)
          {
            this.grid[i][cols[j]].num = shuffledNumbers[j][counter];
            counter++;
          }
        }
      }
    
      fillSeventhSquare(isTop, isLeft){
        let cols = [3, 4, 5];
        let rows = [6, 7, 8];
    
        if(!isTop){
          rows = [0,1,2];
        }
        
        let perfectlyFittedArray = [];
        let attempts:number = 0;
        
        for(let i:number = 0; i < 3; i++)
        {
          attempts = 0;
          perfectlyFittedArray = [];
          while(perfectlyFittedArray.length < 3 && attempts < 3)
          {
            //Grab Rows:
            let row0 = this.LU.removeZeros(this.grabRow(rows[0]));
            let row1 = this.LU.removeZeros(this.grabRow(rows[1]));
            let row2 = this.LU.removeZeros(this.grabRow(rows[2]));
            
            //Col 0:
            //Find remaining in col:
            let remainingInCol = this.LU.shuffle(this.LU.returnRemainingInRowOrCol(this.LU.removeZeros(this.grabCol(cols[i]))));
            // Fit into available arrays:
            perfectlyFittedArray = this.LU.orderRemainingNumbers(remainingInCol, row0, row1, row2);
      
            if(perfectlyFittedArray.length < 3)
            {
              //Reshuffle 5th Square:
              this.reshuffleThirdSquare(isTop, isLeft);
            }
            attempts++;
          }
          let counter:number = 0;
          for(let j:number = rows[0]; j < (rows[2]+1); j++)
          {
              this.grid[j][cols[i]].num = perfectlyFittedArray[counter];
              counter++;
          }
        }
      }
    
      fillEigthSquare(isTop, isLeft){
        let rows = [3, 4, 5];
        let cols =  [6, 7, 8];
    
        if(!isLeft){
          cols = [0, 1, 2];
        }
    
        let perfectlyFittedArray = [];
        let attempts:number = 0;
        
        for(let i:number = 0; i < 3; i++)
        {
          attempts = 0;
          perfectlyFittedArray = [];
          while(perfectlyFittedArray.length < 3 && attempts < 3)
          {
            //Grab Cols:
            let col0 = this.LU.removeZeros(this.grabCol(cols[0]));
            let col1 = this.LU.removeZeros(this.grabCol(cols[1]));
            let col2 = this.LU.removeZeros(this.grabCol(cols[2]));
    
            //Col 0:
            //Find remaining in row:
            let remainingInRow = this.LU.shuffle(this.LU.returnRemainingInRowOrCol(this.LU.removeZeros(this.grabRow(rows[i]))));
            // Fit into available arrays:
            perfectlyFittedArray = this.LU.orderRemainingNumbers(remainingInRow, col0, col1, col2);
      
            if(perfectlyFittedArray.length < 3)
            {
              //Reshuffle 5th Square:
              this.reshuffleFifthSquare(isTop, isLeft);
            }
            attempts++;
          }
          let counter:number = 0;
          for(let j:number = cols[0]; j < (cols[2]+1); j++)
          {
              this.grid[rows[i]][j].num = perfectlyFittedArray[counter];
              counter++;
          }
        }
      }

      newFillMiddleSquare(isTop, isLeft){
        let innerSquareColStart:number = 3;
        let innerSquareColEnd:number = 5;
        let innerSquareRowStart:number = 3;
        let innerSquareRowEnd:number = 5;
        let crucialColNumber:number = 5;
        let crucialRowNumber:number = 5;
  
        let finalPlace = [5, 5];
        let outerRow:number = 5;
        let outerCol:number = 5;
  
        if(!isTop) {
          crucialRowNumber = 3;
          innerSquareRowStart = 4;
          innerSquareRowEnd = 6;
          finalPlace[0] = 3;
          outerRow = 3;
        }
  
        if(!isLeft){
          crucialColNumber = 3;
          innerSquareColStart = 4;
          innerSquareColEnd = 6;
          finalPlace[1] = 3;
          outerCol = 3;
        }
        
        //Grab crucial col & row:
        let combinedCrucialNumbers = [];
        let crucialCol = this.LU.removeZeros(this.grabCol(crucialColNumber));
        let crucialRow = this.LU.removeZeros(this.grabRow(crucialRowNumber));
  
        //Combine into array and shuffle
        for(let i:number = 0; i < crucialCol.length; i++)
        {
          combinedCrucialNumbers.push(crucialCol[i]);
          combinedCrucialNumbers.push(crucialRow[i]);
        }
        combinedCrucialNumbers = this.LU.shuffle(combinedCrucialNumbers);
  
        //Place on inner Square on sudoku where it fits:
        let placedNum:boolean = false;
        let innerSquare:any = [];
        for(let i:number = innerSquareRowStart; i < innerSquareRowEnd; i++)
        {
           for(let j:number = innerSquareColStart; j < innerSquareColEnd; j++)
           {
              while(!placedNum)
              {
                 let squareArray = this.retrieveSquareArray(4, 4, false, false);
                 let rowArray = this.LU.removeZeros(this.grabRow(i));
                 let colArray = this.LU.removeZeros(this.grabCol(j));
                 let suitableNum = this.findSuitableNumber(squareArray, colArray, rowArray, combinedCrucialNumbers);
                 //If no suitable number, randomize an available:
                 if(suitableNum == 0)
                 {
                    let newPotentialNumbers = this.LU.returnMissingNumbersFromRowAndCol(rowArray, colArray);
                    suitableNum = this.findSuitableNumber(squareArray, colArray, rowArray, newPotentialNumbers);
                 }
                 this.grid[i][j].num = suitableNum;
                 innerSquare.push(suitableNum);
                 placedNum = true;
              }
              placedNum = false;
           }
        }
  
        //OuterSection from innersquare:
        //Place remaining 1 or 2 numbers from col 5 on outerSection row:
        let outerRowPotentials = this.LU.returnPotentialOuterNumbers(this.retrieveSquareArray(4, 4, false, false), crucialCol);
        
        outerRowPotentials = this.tryPlaceNumbers(outerRowPotentials, outerRow, innerSquareColStart);
        this.tryPlaceNumbers(outerRowPotentials, outerRow, innerSquareColStart + 1);
  
        //Place remaining 1 or 2 numbers from row 5 on outerSection col:
        let outerColPotentials = this.LU.returnPotentialOuterNumbers(this.retrieveSquareArray(4, 4, false, false), crucialRow);
        outerColPotentials = this.tryPlaceNumbers(outerColPotentials, innerSquareRowStart, outerCol);
        this.tryPlaceNumbers(outerColPotentials, innerSquareRowStart+1, outerCol);
  
        //Place Last Number:
        let squareArrayFinal = this.retrieveSquareArray(4, 4, false, false);
        this.grid[finalPlace[0]][finalPlace[1]].num = this.LU.findLastNumberInSquareArray(squareArrayFinal);
    }
  

    
      fillFinalSquare(isTop, isLeft){
        let rows = [6,7,8];
        let cols = [6,7,8];
        if(!isTop){
          rows = [0,1,2];
        }
        if(!isLeft){
          cols = [0,1,2];
        }
        let perfectlyFittedArray = [];
        let attempts:number = 0;
        
        for(let i:number = 0; i < 3; i++)
        {
          attempts = 0;
          perfectlyFittedArray = [];
          while(perfectlyFittedArray.length < 3 && attempts < 3)
          {
            //Grab Cols:
            let col0 = this.LU.removeZeros(this.grabCol(cols[0]));
            let col1 = this.LU.removeZeros(this.grabCol(cols[1]));
            let col2 = this.LU.removeZeros(this.grabCol(cols[2]));
    
            //Col 0:
            //Find remaining in row:
            let remainingInRow = this.LU.shuffle(this.LU.returnRemainingInRowOrCol(this.LU.removeZeros(this.grabRow(rows[i]))));
            // Fit into available arrays:
            perfectlyFittedArray = this.LU.orderRemainingNumbers(remainingInRow, col0, col1, col2);
      
            if(perfectlyFittedArray.length < 3)
            {
              //Reshuffle 5th Square:
              this.reshuffleFifthSquare(isTop, isLeft);
            }
            attempts++;
          }
          let counter:number = 0;
          for(let j:number = cols[0]; j < (cols[2]+1); j++)
          {
              this.grid[rows[i]][j].num = perfectlyFittedArray[counter];
              counter++;
          }
        }
      }

      findSuitableNumber(squareArray, colArray, rowArray, potentialNumbers){
        for(let i:number = 0; i < potentialNumbers.length; i++)
        {
          if(!squareArray.includes(potentialNumbers[i]) && !colArray.includes(potentialNumbers[i]) && !rowArray.includes(potentialNumbers[i]))
          {
            return potentialNumbers[i];
          }
        }
      return 0;
    }

    tryPlaceNumbers(numbersToPlace, rowCoord, colCoord){
        let col = this.LU.removeZeros(this.grabCol(colCoord));
        let row = this.LU.removeZeros(this.grabRow(rowCoord));
        let squareArray = this.retrieveSquareArray(4, 4, false, false);
        let placedNum:boolean = false;
    
        for(let i:number = 0; i < numbersToPlace.length; i++)
        {
          if(!col.includes(numbersToPlace[i]) && !row.includes(numbersToPlace[i])){
            this.grid[rowCoord][colCoord].num = numbersToPlace[i];
            placedNum = true;
            numbersToPlace.splice(i, 1);
          }
        }
        if(placedNum == false)
        {
          let randomNum:number = this.LU.returnRandomNumberThatFits(row, col, squareArray);
          this.grid[rowCoord][colCoord].num = randomNum;
        }
        return numbersToPlace;
      }
    
      findRemainingInRow(row){
        let rowArray = this.grabRow(row);
        //Get remaining numbers to be placed in current row:
        let remainingInRow = this.LU.returnRemainingInRowOrCol(rowArray);
        return remainingInRow;
      }
    
      findRemainingInCol(col){
        let colArray = this.grabCol(col);
        let remainingInCol = this.LU.returnRemainingInRowOrCol(colArray);
        return remainingInCol;
      }
      retrieveFirstRow(startRow, startCol, endCol){
        let firstRow = [];
        for(let i:number = startCol; i < endCol; i++)
        {
          firstRow.push(this.grid[startRow][i].num);
        }
        return firstRow;
      }
      retrieveFirstCol(startCol, startRow, endRow){
        let firstCol = [];
        for (let i:number = startRow; i < endRow; i++)
        {
          firstCol.push(this.grid[i][startCol].num);
        }
        return firstCol;
      }
    
      retrieveLastRow(endRow, startCol, endCol){
        let lastRow = [];
        for(let j:number = startCol; j < endCol; j++)
        {
          lastRow.push(this.grid[endRow-1][j].num);
        }
        return lastRow;
      }
    
      retrieveLastCol(endCol, startRow, endRow){
        let lastCol = [];
        for(let i:number = startRow; i < endRow; i++)
        {
          lastCol.push(this.grid[i][endCol].num);
        }
        return lastCol;
      }
    
      grabRow(row){
        let rowArray = [];
        for (let i:number = 0; i < 9; i++)
        {
          rowArray.push(this.grid[row][i].num);
        }
        return rowArray;
      }
    
      grabCol(col){
        let colArray = [];
        for(let i:number = 0; i < 9; i++)
        {
          colArray.push(this.grid[i][col].num);
        }
        return colArray;
      }

      retrieveSquareArray(row, col, retrieveOnlyHidden, retrieveNullNumbers){
        let squareArray = [];
        let startRow:number = null;
        let endRow:number = null;
        let startCol:number = null;
        let endCol:number = null;
    
        let rowStartAndEnd = this.LU.fetchRowOrCol(row);
        startRow = rowStartAndEnd[0];
        endRow = rowStartAndEnd[1];
    
        let colStartAndEnd = this.LU.fetchRowOrCol(col);
        startCol = colStartAndEnd[0];
        endCol = colStartAndEnd[1];
    
        for(let i:number = startRow; i < endRow; i++)
        {
          for(let j:number = startCol; j < endCol; j++)
          {
            if(retrieveOnlyHidden){
                if(this.grid[i][j].isHidden)
                  squareArray.push(this.grid[i][j].num);
            }
            else if(retrieveNullNumbers){
              if(this.grid[i][j].isHidden)
              {
                squareArray.push(null);
              }
              else {
                squareArray.push(this.grid[i][j].num);
              }
            }
            else{
              squareArray.push(this.grid[i][j].num);
            }
          }
        }
        return squareArray;
      }

      retrieveSquareSudokuNumbers(row, col){
        let squareArray = [];
        let startRow:number = null;
        let endRow:number = null;
        let startCol:number = null;
        let endCol:number = null;
    
        let rowStartAndEnd = this.LU.fetchRowOrCol(row);
        startRow = rowStartAndEnd[0];
        endRow = rowStartAndEnd[1];
    
        let colStartAndEnd = this.LU.fetchRowOrCol(col);
        startCol = colStartAndEnd[0];
        endCol = colStartAndEnd[1];
    
        for(let i:number = startRow; i < endRow; i++)
        {
          for(let j:number = startCol; j < endCol; j++)
          {
            let tempSudokuNumber:Sudokunumber = new Sudokunumber(this.solutionsGrid[i][j].num);
            tempSudokuNumber.isHidden = this.grid[i][j].isHidden;
            squareArray.push(tempSudokuNumber);
          }
        }
        return squareArray;
      }
  


          isCoordInStartGrid(row, col){
            switch(this.startGrid){
              case startingGrid.TOPLEFT: {
                if(row >= 0 && row < 3){
                  if(col >= 0 && col <= 3){
                    return true;
                  }
                }
                return false;
              }
              case startingGrid.TOPRIGHT: {
                if(row >= 0 && row < 3){
                  if(col >= 6 && col < 9){
                    return true;
                  }
                } 
                return false;
              }
              case startingGrid.BOTTOMLEFT: {
                if(row >= 6 && row < 9){
                  if(col >= 0 && col < 3){
                    return true;
                  }
                }
                return false;
              }
              case startingGrid.BOTTOMRIGHT: {
                if(row >= 6 && row < 9){
                  if(col >= 6 && col < 9){
                    return true;
                  }
                }
                return false;
              }
            }
          }
          solveGame(){
            //Step 1: Find position of next free cell in grid:
            let coords:any = [];
            coords = this.findNextFreeCell();
        
            //Step 1.2: If no free cell, return true:
            if(coords.length == 0){
              this.solutionsCount++;
              // this.drawLettersOnGrid();
              console.log(this.solutionsCount + ' many solutions');
              return;
              
            }
        
            //Step 2: Try place potential number:
            let row = this.LU.removeZeros(this.grabRow(coords[0]));
            let col = this.LU.removeZeros(this.grabCol(coords[1]));
            let squareArray = this.retrieveSquareArray(coords[0], coords[1], false, false);
            let potentialNumbers = this.LU.returnArrayOfNumbersThatFit(row, col, squareArray);
            
            for(let i:number = 0; i < potentialNumbers.length; i++)
            {
               this.grid[coords[0]][coords[1]].num = potentialNumbers[i];
               this.grid[coords[0]][coords[1]].isHidden = false;
        
               //Return if success, yay:
               if(this.solveGame()){
                 return this.solutionsCount;
               }
        
               //Failure: unmake, try again:
               this.grid[coords[0]][coords[1]].num = 0;
               this.grid[coords[0]][coords[1]].isHidden = true;
            }
            return false;
          }

          findNextFreeCell(){
            let freeCell:any = [];
            for(let i:number = 0; i < 9; i++)
            {
              for(let j:number = 0; j < 9; j++)
              {
                 if(this.grid[i][j].num == 0){
                   freeCell.push(i);
                   freeCell.push(j);
                   return freeCell;
                 }
              }
            }
            return freeCell;
          }
}
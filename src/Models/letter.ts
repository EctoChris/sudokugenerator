export class Letter {

   //Class Data Members:
   highlighted:boolean;
   hovered:boolean;
   inWord:boolean;
   private char;
   public row:number;
   public col:number;

  constructor(row:number, col:number)
  {
    this.highlighted = false;
    this.hovered = false;
    this.inWord = false;
    this.char = "0";
    this.row = row;
    this.col = col;
  }

  highlight()
  {
    this.highlighted = true;
  }

  getHigh()
  {
    return this.highlighted;
  }

  getInWord()
  {
    return this.inWord;
  }

  getHover()
  {
    return this.hovered;
  }

  hoverOnLetter()
  {
    this.hovered = true;
  }
  removeHigh()
  {
    this.highlighted = false;
  }
  placeInWord()
  {
    this.inWord = true;
  }

  getChar()
  {
      return this.char;
  }

  setChar(char){
    this.char = char;
  }
  getRow()
  {
    return this.row;
  }
  getCol()
  {
    return this.col;
  }
}

export class Sudokunumber {
    public num:number;
    public isHidden:boolean;

    constructor(num){
        this.num = num;
        this.isHidden = false;
    }
}
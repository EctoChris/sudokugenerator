// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyDqFwED74yAOsS4_WpE_6shiM9HBDy03tE",
    authDomain: "findawordgenerator.firebaseapp.com",
    databaseURL: "https://findawordgenerator.firebaseio.com",
    projectId: "findawordgenerator",
    storageBucket: "findawordgenerator.appspot.com",
    messagingSenderId: "322474448440"
  }
  
};

